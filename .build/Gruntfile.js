module.exports = function(grunt){
  grunt.initConfig({

    sass: {
      dist: {
        options: {
          style: 'compressed'   // nested, compact, compressed, expanded.
        },
        files: {
          '../public/assets/public/styles/main.css': 'styles/main.sass',
        }
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer-core')({
            browsers: ['last 2 versions']
          })
        ]
      },
      dist: {
        src: '../public/assets/public/styles/*.css'
      }
    },

    rucksack: {
      compile: {
        files: {
          '../public/assets/public/styles/main.css': '../public/assets/public/styles/main.css'
        }
      }
    },

    cssmin: {
      options: {
          keepSpecialComments: 0
      },
      target: {
        files: {
          '../public/assets/public/styles/main.css': ['../public/assets/public/styles/main.css']
        }
      }
    },

    babel: {
      options: {
        sourceMap: true,
        presets: ['es2015']
      },
      dist: {
        files: {
          "../public/assets/public/scripts/app.js" : "scripts/app.js"
        }
      }
    },

    uglify: {
      my_target: {
        files: {
          '../public/assets/public/scripts/app.js': ['../public/assets/public/scripts/app.js'],
          '../public/assets/public/scripts/materialize.js': ['../public/assets/public/scripts/materialize.js'],
        }
      }
    },

    watch: {
      sass:
      {
        files: ['styles/*.sass','styles/*/*.sass'],
        tasks: ['sass','rucksack','postcss:dist','cssmin'],
        options: {
          livereload: true,
        }
      },
      js:
      {
        files: ['scripts/*.js','scripts/*/*.js'],
        tasks: ['babel','uglify'],
        options: {
          livereload: true,
        }
      },
      html:
      {
        files: ['../*/*.phtml','../*/*.php','../apps/*/views/*.phtml','../apps/*/views/*/*.phtml'],
        options: {
          livereload: true,
        }
      }
    },

  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-rucksack');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.registerTask('default', ['watch']);
}
