<?php
$router->add("/contact", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'contact',
  'action'     => 'index'
])->via(["POST"]);

$router->add("/english", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'english'
]);

$router->add("/portuguese", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'portuguese'
]);
