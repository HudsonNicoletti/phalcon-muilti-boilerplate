<?php

namespace Website\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  protected $website_lang;

  public function initialize()
  {
    $this->assets
    ->addCss('assets/public/styles/main.css')
    ->addJs('https://use.fontawesome.com/2ca130448b.js', false)
    ->addJs('assets/public/scripts/jquery.min.js')
    ->addJs('assets/public/scripts/wow.min.js')
    ->addJs('assets/public/scripts/scrollIt.min.js')
    ->addJs('assets/public/scripts/materialize.js')
    ->addJs('assets/public/scripts/app.js');

    # verify language cookie , if doesn't exist then create it
    ($this->cookies->has("website_lang")) ? $this->website_lang = $this->cookies->get("website_lang")->getValue() : $this->setLang();

  }

  protected function setLang()
  {
    $lang = substr($this->request->getBestLanguage(),0,2);
    switch($lang)
    {
      case 'pt': $this->cookies->set("website_lang",$lang); break;
      case 'en': $this->cookies->set("website_lang",$lang); break;
      default  : $this->cookies->set("website_lang","en");
    }
    $this->Website_lang = $this->cookies->get("website_lang")->getValue();
  }

}
