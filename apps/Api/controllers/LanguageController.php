<?php

namespace Api\Controllers;

use Api\Misc\ApiExceptions as ApiExceptions;

use Phalcon\Mvc\Controller;

class LanguageController extends ControllerBase
{
  public function ChangeAction()
  {
    switch($this->dispatcher->getParam("language"))
    {
      case 'portuguese': $this->cookies->set("website_lang","pt"); break;
      case 'english':    $this->cookies->set("website_lang","en"); break;
    }

    $this->response->redirect("/");
  }
}
