# PhalconPHP Multi Application Boilerplate

This is a skeleton for a multi application based on Phalcons auto generated templates, modified by me.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- PHP7
- PhalconPHP ( 3.4 ONLY !)
- ComposerPHP
- NPM ( optional )

### Installing

install dependencies for composer

```
cd libraries
composer install
```

install npm dependencies

```
cd .build
npm install

npm i -g grunt-cli
```

## Built With

* [PhalconPHP](https://phalconphp.com/en/)

## Authors

* **Hudson Nicolletti** - *Initial work* - [PhalconPHP](https://olddocs.phalconphp.com/en/3.0.1/reference/applications.html)
